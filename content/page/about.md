---
title: About me
comments: false
---

Hello, my name is Burkey.

- work | hashicorp
- products | consul terraform vault nomad
- contributor and author | powernsx
- play | soccer
- follow | AFL
- location | australia
- interests | lord of the rings, wheel of time, star wars, boardgames, video games

