---
title: "Nomad Nugget - 001"
date: 2020-10-07
comments: false
---

![](/nomad.png)
# Nomad Notes
Nomad Notes are a bi-weekly or so update of all things happening with scheduling application run times with HashiCorp Nomad. There may be blog entries, links to great content, or simply my hot takes on a specific element of the ecosystem. It will cover both Open Source and Enterprise Nomad so there should be something for everyone.

### [Cloudflare and Nomad](https://blog.cloudflare.com/how-we-use-hashicorp-nomad/)

Cloudflare are known for the protectors of the internet and deliver services that help democratise the internet. There is a substantial portion of the internet that runs through Cloudflare. Nomad sits at their edge and within their DC's.

Reading between the lines of their deployment they're utilising the placement logic of the clients and in addition to that, the jobs themselves. By using `constraint` they can attest to what rack, node, node type, hardware attribute (memory, cpu, GPU, etc), that the node must have before a `job` is scheduled.

Pretty great testament to the robust and targeted nature of Nomad.

### [Nomad and Cron](https://blog.cloudflare.com/cron-triggers-for-scheduled-workers/)

Another post by Cloudflare on their own accord. Here they are detailing their solution around Cron Tiggers for their Cloudflare Workers. This is their serverless compute platform.

Running cron-triggers allows a task to be periodic and potentially hit an API, target, or perform a task at a scheduled interval. The system that does the tasks is built with `Rust` and is scheduled to Nomad at their edges. Given that Nomad has the ability to perform `health checks`, `canary`, and `lifecycle` operations it gives Cloudflare the option to update, rollback, reschedule, provide availability to their new service offering.

### [HashiConf Digital: Nomad Keynote](https://digital.hashiconf.com/schedule/day-3)

Here's the blurb from the session `"Join the Nomad team, Mitchell, and Armon as they take us through the journey to 1.0 for Nomad, where Nomad falls into the framework of HashiCorp's products."`

What is great about this sessions is Yishan will paint our what we're doing now and where Nomad is going. Especially given how close 1.0 is - he will outline the remaining steps to take Nomad to a full release. It will be a good use of time. For me this session is Thursday 0730 AEST but log into the HashiConf platform and get it localised for your timezone.

### [First Class Federation](https://www.hashicorp.com/resources/first-class-federation-with-nomad)

Here's a HashiCorp Snapshot with yours truly. A small, punchy, 15 minute session that demonstrates the use case around Nomad Federation for scheduling workloads across different clusters in different region. Simple, effective, and straight forward.

### The Wrap

There is a lot of great things happening in the Scheduler space with regards to Nomad. Do you want to hear more? Is there a topic you want to see me cover? Reach out to me on [Twitter](https://twitter.com/pandom_) and I'll see what I can do.

/Burkey