---
title: Permissions on AWS key
date: 2019-10-28
comments: false
---

I needed a new key pair for my AWS account. I encountered this error after creating a new AWS Key Pair.

```
ssh -i ~/Desktop/burkey.pem ubuntu@52.64.228.25           
The authenticity of host '52.64.228.25 (52.64.228.25)' can't be established.
ECDSA key fingerprint is SHA256:UxRi6jxC4zljwQrpPPgEKo5NL2Y5kCp5PIrVNU0HpJ4.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '52.64.228.25' (ECDSA) to the list of known hosts.
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for '/Users/aburke/Desktop/burkey.pem' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "/Users/aburke/Desktop/burkey.pem": bad permissions
ubuntu@52.64.228.25: Permission denied (publickey).
```

The files permissions of 600 mean that the owner has full read and write access to the file, while no other user can access the file. Permissions of 644 mean that the owner of the file has read and write access, while the group members and other users on the system only have read access.

A quick `chmod 600 ~/Desktop/burkey.pem` and we're cooking with gas.
