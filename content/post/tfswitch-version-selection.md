---
title: tfswitch - Making version selection easy
date: 2020-01-07
comments: false
---
## Version swapping made easy

Are you a user of Terraform? You are? Great. [Khajitt](https://knowyourmeme.com/memes/khajiit-khajiit-has-wares) has wares - if you have coin. Elder Scroll jokes aside - Let me tell you about `tfswitch`. This handy little application allows you to swap versions of Terraform used by your local system without having to uninstall/reinstall a specific version for a manifest.

Why would I want to use `tfswitch`?

- A Version Control System triggers a webhook for a Terraform manifest that needs Terraform to be set to a specific version
- The workflow and/or operator has to deal with using various versions (0.11 and 0.12)
- You are sick of changing versions locally with homebrew, apt, or choclatey

Grab `tfswitch` via homebrew on macOS or other platforms linked at the end of this post.

## Time to play switch-a-roo

**List all Terraform versions**

```
tfswitch -l                                                           ↵ INT
Use the arrow keys to navigate: ↓ ↑ → ←
? Select Terraform version:
  ▸ 0.12.19 *recent
    0.12.17 *recent
    0.11.14 *recent
    0.12.19
↓   0.12.18
```
A list of all Terraform versions is displayed allowing for selection of a specific release. 

**Selecting a new version**
```

✔ 0.12.19
Downloading https://releases.hashicorp.com/terraform/0.12.19/terraform_0.12.19_darwin_amd64.zip to terraform_0.12.19_darwin_amd64.zip
Downloading ...
17101269 bytes downloaded.
Switched terraform to version "0.12.19"
```

When selecting a new version note that it will go off and download it.

**Selecting an existing binary**
```
tfswitch 0.11.12
Switched terraform to version "0.11.12"
```
If it already exists then it is a quick switch without the download. As you can see everything is neatly changed to the correct version.

**Switching between with ease**
```
terraform -v
Terraform v0.11.12
```
Switching back is just as easy
```
~ ❯ tfswitch 0.12.19
Switched terraform to version "0.12.19"
~ ❯ terraform -v
Terraform v0.12.19
```
This definitely beats installing specific versions or aliasing the binaries.

## Using tfswitch

If you're using Terraform Cloud or Terraform Enterprise then your workspace can be configured to consume a [specific version of Terraform](https://www.terraform.io/docs/cloud/workspaces/settings.html). If you're not, save yourself some headaches and grab [tfswitch](https://warrensbox.github.io/terraform-switcher/).