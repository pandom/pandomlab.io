---
title: "Nomad Stanzas"
date: 2021-01-13
comments: false
---

![](/nomad.png)
# Nomad Stanzas got me good!

Nomad has configuration for Server, Clients, and Jobs. These job spec or HCL configuration files are made up of one or many stanzas. Whilst configuring a few job specifications the other day I couldn't figure why a specific feature was throwing an error. Then I realised something about it's placement.

Within the documentation for all configuration there is a `Placement` field. This filed denotes at what level of nesting are you configuring your feature. Let's use the [affinity](https://www.nomadproject.io/docs/job-specification/affinity) stanza as an example and explore the placements.

Note below the Placement hierarchy.

```
job -> affinity
job -> group -> affinity
job -> group -> task -> affinity
```

This means as an administrator or developer I can configure Nomad Affinity functionality at the job, group, or task level. This allows an element of control. The first few times reading over the document it didn't click with me. What does this look like at a code level?


```
job "docs" {
  # Prefer nodes in the ap-southeast-2 datacenter
  affinity {
    attribute = "${node.datacenter}"
    value     = "ap-southeast-2"
    weight    = 100
  }

  group "example" {
    # Prefer the "r1" rack
    affinity {
      attribute  = "${meta.rack}"
      value     = "r1"
      weight    = 50
    }

    task "server" {
      # Prefer nodes where "my_custom_value" is greater than 5
      affinity {
        attribute = "${meta.my_custom_value}"
        operator  = ">"
        value     = "3"
        weight    = 50
      }
    }
  }
}
```

This example has 3 affinities that are applied. Note that `Placement` field at the top of the documentation is used. `job -> affinity` denotes affinity configuration under `job` whilst `job -> group -> affinity` applies to affinity configuration under the `group` stanza.

Quick one but a gotcha that made me view the documentation in a whole new light.