---
title: Learning Nomad - Introduction
date: 2020-04-02
comments: false
---

![](/nomad.png)
### The minimalist scheduler

This is the introduction to an informal blog series about how I am getting up and running with HashiCorp Nomad. The goal is to be an informal journey of applying concepts and building upon them. This will definitely be full of great things and without a doubt less than ideal approaches initially. I know for the first few posts I already have learnt about better ways of doing things and subsequent posts will improve.

So a couple of things:
- We will use real applications. Java Runtimes, Containers, et el. No Wordpress/wp-admin pages will grace these posts.
- It won't be perfect and each post whilst self contained, will have questions and discussions for me to improve on
- It will be focusing on elements of Nomad and Nomad Enterprise. I will endeavour to call out if I use an Enterprise feature and mark it with `Nomad Enterprise` or reference it

So join me on this series and I would be remiss if I didn't quote one of my favorites. As one little Hobbit who lived under a hill once said "I'm going on an adventure"

-Burkey