---
title: "Terraform locals and provider interpolation"
date: 2020-04-14
comments: false
---

### Provider configurations

There are times when HCL provides interpolation - the calculation of dynamic elements and variables - with use in the code. There are times where this doesn't work much to my sadness and this is when a variable is used in a provider configuration.

Such cases are like below:

```
provider "nomad" {
  address = http://my.nomad.cluster:4646
  region  = "ap-southeast-2"
}
```

These values are static and are used when an environment is initalised. Interpolation cannot occur on a provider for order of operation reasons. After chatting with the one and only Grant Orchard - he suggested I visit locals. 

### Going Local

Locals allow me to assign a name to an expression. What I wanted to achieve was that many demonstration environments I use have different ingress DNS entries for the Amazon ELBs. That's fine. I can address this randomness by reading off an existing statefile. Using locals allows me to refer to it simply as `local.address`

```
locals {
    address = data.terraform_remote_state.cloud-nomad.outputs.addresses.nomad_ui
}
```
It can then be referenced many times in the same mainfest. I can define my address for my provider which needs the Nomad UI as `local.address`.

```
provider "nomad" {
  address = local.address
  region  = "ap-southeast-2"
}
```
Nice and simple. This works a charm and allows me to subsequently use terraform files for operator configuration in Nomad when I may not know the environment details. This allows RAPID iteration of my code across different environments by overriding variables!

Huzzah!


