---
title: My publishing workflow
date: 2020-02-04
comments: false
---

There is usually a reason why many people hunker down on a technology. There is a decision point, a problem space, or a justification. So why did I decide to start my blog anew and consume a drastically different workflow compared to that of Wordpress?

Because others were. The "Cool Kids (tm)" were all using GitHub with markdown and static sites for their blogs and it was appealing. I used markdown a lot for technical note taking and it's a logical step So in the end this resulted in using a Hugo theme for static site generation. This is a ruby framework that builds and pre-renders a site. Netlify hosts this and builds the site for me as changes are made. It will recompose my website and redeploy the latest build.

So how does my workflow improve things?

**The Workflow**

I own the domain you're reading against now. This is called `burkey.dev`. This domain I managed to secure after looking for something new. At HashiCorp, my email, alias, and preferred name are all Burkey. Only my mother really calls me 'Anthony' and that's when I am in trouble!

Alright - so lets look at this workflow

![](/2020-02-04-12-00.png)
_Figure 1 - The workflow visualised_

All my writing is done on VScode in markdown. This allows me to quickly write my blogs without worrying if a WYSIWYG editor will mangle or deform the output. From there, once I am happy with my writing I will commit the change and push it to GitHub.

Netlify has a web hook configured to watch my repository. When any change occurs it will trigger a build of all assets in the repo. These assets are used to build the static site. When this is done (30-45 seconds) my website has my new content ready to view.

![](/2020-02-04-12-02.png)
_Figure 2 - Build steps_

**Branch Based subdomains**

So what was second row in _Figure 1_? It was a branch based repository generating a subdomain.

I had planned to co mingle content on this site. traditionally I would have the notion of a `Full Feed` and then have `tags` that would allow a reader to browse per category. I have a keen interest in tabletop boardgames, fantasy books, and many other things. I came to realise to not cross the streams. I didn't want to buy another domain.

Netlify allows `branch based` subdomains. This features allows `tabletop.burkey.dev` to be sourced and built from the `tabletop` branch stored within the repo whilst `burkey.dev` is from `master`. With a cross linked menu, I have found that I met the requirements without buying heaps of domains or managing numerous deployments. Now I can do my battle reports, write about Star Wars Legion, or anything else, and keep it out of my technical stuff.

**Summary**

Let alone I am not worrying about my backend - I feel the barrier to writing is getting easier. I live a lot of my life in GitHub/GitLab and having my blog now into this is great. It also allows me to keep a copy of all my blogs offline and I can easily edit them. Moving from Wordpress and MarsEdit3 has been great. MarsEdit3 covered a lot of things and bad stuff from Wordpress - but now having markdown and version control support I am in a much happier place.

Oh - did I mention I do not need to worry about CDN, SSL Certificates, or food and water of my Wordpress? 